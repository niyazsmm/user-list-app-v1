import React, { Component } from 'react';
import UserList from './container/UserList/UserList';
import Header from './component/Header/Header';

class App extends Component {
  render() {
    return (
      <div>
        <Header/>
        <UserList />
      </div>
    );
  }
}

export default App;