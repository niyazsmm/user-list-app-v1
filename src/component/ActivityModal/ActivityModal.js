import React, { Component } from 'react';
import { 
    Button,  
    Dialog, 
    DialogTitle, 
    DialogContent, 
    DialogActions,
    DialogContentText,
    Grid,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TextField,
} from "@material-ui/core"

class ActivityModal extends Component {

    state = {
        userInfo: this.props.userInfo,
        activityPeriods: this.props.userInfo.activity_periods,
        displayActivity: [],
        dateToSelect: "",
    }

    componentWillMount() {
        this.getDate();
    }

    componentDidMount() {
        this.findTask()
    }

    getDate() {
        let date = new Date();
        let day = date.getDate();
        let month = date.getMonth() + 1;
        let year = date.getFullYear();
        if (month < 11) month = "0" + month;
        if (day < 10) day = "0" + day;
        let today = year + "-" + month + "-" + day;
        this.setState({
            dateToSelect: today
        })
    }
    
    /***************************** Function to find activity list  *************************************/
    findTask = () => {
        let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];
        
        //=================== Modifying date format to required type to match with api data format
        let selectDateList = this.state.dateToSelect.split("-");
        let month = Math.abs(parseInt(selectDateList[1]))
        let day = Math.abs(parseInt(selectDateList[2]))
      
        if( month < 13 ) {
            selectDateList[1] = months[month - 1]
        }
        let selectDate = `${selectDateList[0]} ${selectDateList[1]} ${day}`;
        
        
        const activityPeriods = [...this.state.activityPeriods];
        const assignedList = [];
        //===========Checking Assigned Task Loop ======================
        for (let i = 0; i < activityPeriods.length; i++) { 
            let sTime = activityPeriods[i].start_time;
            let datatime = sTime.split(" ");
            let date = `${datatime[2]} ${datatime[0]} ${datatime[1]}`;
            if(date === selectDate) {
                console.log(activityPeriods[i])
                assignedList.push(activityPeriods[i]);
            } 
        }

        this.setState({
            displayActivity: assignedList
        }, () => console.log(this.state))


    }

    //=================== handler change
    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        }, () => this.findTask(this.state.dateToSelect) )
    }

    //============== handler close 
    handleClose = () => {
        this.props.handleClose();
    }
    render() {
        const { displayActivity } = this.state;
        return (
            <div>
                <Dialog open={this.props.open} onClose={this.handleClose} aria-labelledby="form-dialog-title" size="lg">
                    <DialogTitle id="form-dialog-title">Activity List</DialogTitle>
                    
                    <DialogContent className="">
                        <Grid item xs={12} sm={12} md={12} lg={12} >
                             <TextField
                                margin="dense"
                                name="dateToSelect"
                                label="Select Date"
                                type="date"
                                fullWidth
                                value = {this.state.dateToSelect}
                                onChange={this.handleChange}
                            />
                        </Grid>
                        {
                            displayActivity.length > 0 ?
                                <TableContainer>
                                    <Table>
                                        <TableHead>
                                            <TableRow >
                                                <TableCell style={{fontWeight: 'bold'}}>
                                                    Start Time
                                                </TableCell>
                                                <TableCell style={{fontWeight: 'bold'}}>
                                                    Start Time
                                                </TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {
                                                displayActivity.map((data, index) => {
                                                    return(
                                                        <TableRow key={index}>
                                                            <TableCell className="py-3">
                                                                {data.start_time}
                                                            </TableCell>
                                                            <TableCell className="py-3">
                                                                {data.end_time}
                                                            </TableCell>
                                                        </TableRow>
                                                    )
                                                })
                                            }
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            :
                            <DialogContentText>
                                No Activity Assigned on Selected Date 
                            </DialogContentText>
                        }
                       
                    </DialogContent>

                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancel
                        </Button>
                    </DialogActions>
                </Dialog>
                
            </div>
        );
    }
}

export default ActivityModal;